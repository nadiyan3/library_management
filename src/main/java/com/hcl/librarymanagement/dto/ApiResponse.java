package com.hcl.librarymanagement.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, Long httpStatus) {

}
