package com.hcl.librarymanagement.dto;

public record BookDto(String bookName,int availableCopies,String authorName,String category) {

}
