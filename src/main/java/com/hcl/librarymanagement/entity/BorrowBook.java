package com.hcl.librarymanagement.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class BorrowBook {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long borrowId;
	@ManyToOne
	private Book bookId;
	@ManyToOne
	private User userId;
	private Status status;
	private LocalDate borrowDate;
	private LocalDate dueDate;

}
