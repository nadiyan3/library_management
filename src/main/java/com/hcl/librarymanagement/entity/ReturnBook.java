package com.hcl.librarymanagement.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class ReturnBook {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long returnId;
	@OneToOne
	private BorrowBook borrowId;
	private LocalDate returnDate;

}
