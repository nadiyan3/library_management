package com.hcl.librarymanagement.exception;

public class UserNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public UserNotFound() {
		super("Resource Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
