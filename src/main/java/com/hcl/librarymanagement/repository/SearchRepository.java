package com.hcl.librarymanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.librarymanagement.entity.Book;

public interface SearchRepository  extends JpaRepository<Book, Long>{

}
